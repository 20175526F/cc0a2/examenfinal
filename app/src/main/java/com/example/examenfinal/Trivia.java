package com.example.examenfinal;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class Trivia {

    Context context;
    List<String> preguntas = new ArrayList<>();
    List<Integer> imagenes = new ArrayList<>();
    List<Boolean> respuestas = new ArrayList<>();

    Trivia(Context current){
        this.context = current;
        preguntas.add(context.getString(R.string.p1));
        preguntas.add(context.getString(R.string.p2));
        preguntas.add(context.getString(R.string.p3));
        preguntas.add(context.getString(R.string.p4));
        preguntas.add(context.getString(R.string.p5));
        preguntas.add(context.getString(R.string.p6));
        preguntas.add(context.getString(R.string.p7));
        preguntas.add(context.getString(R.string.p8));
        preguntas.add(context.getString(R.string.p9));


        imagenes.add(R.drawable.luna);
        imagenes.add(R.drawable.ajigallina);
        imagenes.add(R.drawable.arrozpollo);
        imagenes.add(R.drawable.ceviche);
        imagenes.add(R.drawable.chicharron);
        imagenes.add(R.drawable.tallarines);
        imagenes.add(R.drawable.gato3);
        imagenes.add(R.drawable.mexico);
        imagenes.add(R.drawable.tigre);

        respuestas.add(false);
        respuestas.add(true);
        respuestas.add(true);
        respuestas.add(true);
        respuestas.add(false);
        respuestas.add(false);
        respuestas.add(false);
        respuestas.add(true);
        respuestas.add(false);
    }
    public String getQuestion(int i)
    {
        return preguntas.get(i);
    }

    public int getImage(int i)
    {
        return imagenes.get(i);
    }

    public Boolean getAnswer(int i)
    {
        return respuestas.get(i);
    }

}
