package com.example.examenfinal;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;


public class MainActivity extends AppCompatActivity {

    TextView tv_pregunta;
    ImageView iv_imagen;
    Button btn_verdadero, btn_falso;
    Button btn_sgte, btn_anterior;

    Boolean rptaLocal = true;
    int indicePreguntas = 0;

    RelativeLayout layoutP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_pregunta = findViewById(R.id.tv_pregunta);
        iv_imagen = findViewById(R.id.image_view);
        btn_verdadero = findViewById(R.id.btn_true);
        btn_falso = findViewById(R.id.btn_false);
        btn_sgte = findViewById(R.id.btn_sig);
        btn_anterior = findViewById(R.id.btn_ant);
        layoutP = findViewById(R.id.layoutPrincipal);

        refresh();

        btn_sgte.setOnClickListener(view -> {
            if(indicePreguntas<8){
                indicePreguntas++;
                refresh();
            }
            else{
                indicePreguntas = 0;
                refresh();
            }
        });

        btn_anterior.setOnClickListener(view -> {
            if(indicePreguntas<8 && indicePreguntas>0 ){
                indicePreguntas--;
                refresh();
            }
            else{
                indicePreguntas = 0;
                refresh();
            }
        });

    }

    public void respuestaCorrecta(){
        //tv_pregunta.setText("Buena");
        Snackbar.make(layoutP, R.string.msg_snack_bar_v,Snackbar.LENGTH_INDEFINITE).show();
    }

    public void respuestaIncorrecta(){
        //tv_pregunta.setText("MAla");
        Snackbar.make(layoutP, R.string.msg_snack_bar_f,Snackbar.LENGTH_INDEFINITE).show();
    }

    public void refresh(){
        Trivia t1= new Trivia(this);
        tv_pregunta.setText(t1.getQuestion(indicePreguntas));
        iv_imagen.setImageResource(t1.getImage(indicePreguntas));
        btn_verdadero.setOnClickListener(view -> {
            rptaLocal = true;
            if(rptaLocal == t1.getAnswer(indicePreguntas)){
                respuestaCorrecta();
            }
            else{
                respuestaIncorrecta();
            }
        });

        btn_falso.setOnClickListener(view -> {
            rptaLocal = false;
            if(rptaLocal == t1.getAnswer(indicePreguntas)){
                respuestaCorrecta();
            }
            else{
                respuestaIncorrecta();
            }
        });
    }




}